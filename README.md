# Videokompressor

_Simple UI for batch converting video files._

1. Select folder.
2. Click _convert_.
   + Converts video files in root folder and all subfolders to a new (and smaller) format (specified in `transcoder.txt`; [docs](https://handbrake.fr/docs/en/1.3.0/cli/command-line-reference.html)).

**Note** 
+ Uses [HandBrakeCLI](https://handbrake.fr/downloads2.php) for transcoding. Download and put in `./handbrake`.

# Compiling

+ Uses C++ 17 and [FLTK](https://www.fltk.org/) for UI.
+ Build with CMake, `CMakeLists.txt` provided.
