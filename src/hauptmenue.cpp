#include "hauptmenue.hpp"
#include <FL/Fl_Progress.H>
#include <fstream>

int Hauptmenue::los() {
    window = new Fl_Window(500,300, "Videokompressor");

    // Auswahl Eingabeordner
    {
        txt_input = new Fl_Box(start_x, get_y(1), size_x, size_y, "Bitte Ordner auswählen");
        Fl_Button* btn_input = new Fl_Button(start_x + size_x + padding, get_y(1), size_x, size_y, "Ordner Auswählen");
        btn_input->callback(cb_auswahl_input);
    }

    // Los
    {
        btn_los = new Fl_Button(start_x, get_y(3), size_x, size_y, "Kompression Starten");
        btn_los->callback(cb_los, txt_input);
    }

    window->end();
    window->show();
    return Fl::run();
}

unsigned Hauptmenue::count_video_files(const std::string& dir) {
    unsigned n = 0;
    std::cout << __func__ << std::endl;
    for (const std::filesystem::directory_entry& path : std::filesystem::recursive_directory_iterator(ordner)) {
        try {
            if (path.is_regular_file()) {
                std::string pfad(path.path().string());
                std::string ext(path.path().extension().string());
                std::transform(ext.begin(), ext.end(), ext.begin(), [](unsigned char c){ return std::tolower(c); });

                // Videodatei?
                if (ext.find(".mp4") != std::string::npos ||
                    ext.find(".mov") != std::string::npos ||
                    ext.find(".avi") != std::string::npos) {
                    n++;
                }
            }
        }
        catch (...) {}
    }
    return n;
}

void Hauptmenue::cb_auswahl_input(Fl_Widget* w, void* data) {
    Fl_Native_File_Chooser nfc;
    nfc.title("Ordner Auswählen");
    nfc.type(Fl_Native_File_Chooser::BROWSE_DIRECTORY);
    switch (nfc.show()) {
        case -1: std::cerr << nfc.errmsg() << '\n'; break;
        case  1: break; // Cancel
        default:
            std::cout << nfc.filename() << '\n';
            ordner = nfc.filename();
            txt_input->label(ordner.c_str());
            break;
    }
}

void Hauptmenue::cb_los(Fl_Widget* w, void* data) {
    std::cout << __func__ << std::endl;

    // Transcoding auslesen
    std::string transcoding;
    std::ifstream in("transcoding.txt");
    if (in.good()) {
        std::getline(in, transcoding);
        std::cout << "Transcoding = " << transcoding << '\n';
    }
    else {
        std::cerr << "transcoding.txt fehlt\n";
        return;
    }

    const unsigned n_videos = count_video_files(ordner);
    std::cout << n_videos << " Videos gefunden\n";
    unsigned n_done = 0;

    // Fortschrittsbalken hinzufügen
    btn_los->deactivate();
    Fl::check();
    window->begin();
    Fl_Progress* progress = new Fl_Progress(start_x, get_y(2), size_x*2 + padding, size_y);
    progress->minimum(0);
    progress->maximum(1);
    window->end();

    for (const std::filesystem::directory_entry& path : std::filesystem::recursive_directory_iterator(ordner)) {


        try {
            if (path.is_regular_file()) {
                std::string pfad(path.path().string());
                std::string ext(path.path().extension().string());
                std::transform(ext.begin(), ext.end(), ext.begin(), [](unsigned char c){ return std::tolower(c); });

                // Videodatei?
                if (ext.find(".mp4") != std::string::npos ||
                    ext.find(".mov") != std::string::npos ||
                    ext.find(".avi") != std::string::npos) {
                    std::cout << "# Datei gefunden:\n";

                    // Fortschritt aktualisieren
                    progress->value((float)n_done / (float)n_videos);
                    char percent[10];
                    sprintf(percent, "%d%%", int(((float)n_done / (float)n_videos)*100.0));
                    progress->label(percent);
                    n_done++;
                    Fl::check();

                    // Ausgabedatei
                    std::string pfad_out(pfad.begin(), pfad.end() - 4);
                    pfad_out += "k" + ext;

                    // Befehl erstellen
                    std::string command;
                    command += "\"\"";
                    command += std::filesystem::current_path().string() + "/handbrake/HandBrakeCLI\"";
                    command += " -i \"" + pfad + "\"";
                    command += " -o \"" + pfad_out + "\"";;
                    command += " -e x264 -q 20 -B 160";
                    command += "\"";

                    // Debug cout
                    std::cout << "\tPfad= " << pfad << '\n';
                    std::cout << "\tOut = " << pfad_out << '\n';
                    std::cout << "\tExt = " << ext << '\n';
                    std::cout << "\tCmd = " << command << '\n';

                    // Befehl ausführen
                    system(command.c_str());
                }
            }
        }
        catch (...) { /* egal */ }
    }

    // Fortschrittsbalken aufräumen
    window->remove(progress);
    delete progress;
    btn_los->activate();
    window->redraw();
}
