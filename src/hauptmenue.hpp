#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Native_File_Chooser.H>
#include <string>
#include <iostream>
#include <filesystem>

class Hauptmenue final {

public:

    Hauptmenue() {}

    int los();

private:

    static unsigned count_video_files(const std::string& dir);

    static void cb_auswahl_input(Fl_Widget* w, void* data);

    static void cb_los(Fl_Widget* w, void* data);

    static inline std::string ordner;

    static inline Fl_Window* window;

    static inline Fl_Button* btn_los;

    static inline Fl_Box* txt_input;

    // Layout
    static constexpr int padding = 20;
    static constexpr int start_x = 50;
    static constexpr int size_x = 200;
    static constexpr int size_y = 50;
    static constexpr int y = 50;
    static int get_y(int reihe) { return y*reihe + 2 * (reihe-1) * padding; };

};